Características especiales que tienen las licitaciones, 
generalmente hay alguna resolución u otra normativa que la acompaña pero no siempre. 
En los otros casos es una característica de la licitación que se quiere resaltar.